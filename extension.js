'use strict';

const { main:Main } = imports.ui;
const { St, Clutter, Pango } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Self = ExtensionUtils.getCurrentExtension();

const shellVersion = imports.misc.config.PACKAGE_VERSION;


// I use object pattern as main extension, but i avoid to use "this" to make this extension won't
// increase memory usage
class Extension {
    constructor() { }

    _add() {
        // Run only when current session mode is in "unlock-dialog"
        if (Main.sessionMode.currentMode == 'unlock-dialog') {
            // Settings schema
            let settings = ExtensionUtils.getSettings();

            // Clock stack from screenShield
            let clockStack = Main.screenShield._dialog._clock;

            // ScrollView (make it message scrollable)
            let scrollView = new St.ScrollView({
                hscrollbar_policy: St.PolicyType.NEVER,
                vscrollbar_policy: St.PolicyType.EXTERNAL,
                clip_to_allocation: true,
                width: 240, // Recommended size
                height: 48
            });

            // Box layout (because it's scrollable)
            let boxLayout = new St.BoxLayout({
                x_align: Clutter.ActorAlign.CENTER,
            })

            // Message Label
            let message = new St.Label({
                text: settings.get_string('message'),
                x_align: Clutter.ActorAlign.CENTER,
                opacity: 255 * 0.6, // A bit low of opacity
            });
            boxLayout.add_child(message);

            // Add message to scrollView
            scrollView.add_actor(boxLayout);

            // Append scroll view above date label from clockStack
            clockStack.insert_child_above(scrollView, clockStack._date);

            // Set clutter text from message
            message.clutter_text.set({
                line_alignment: Pango.Alignment.CENTER,
                line_wrap: true,
                line_wrap_mode: Pango.WrapMode.WORD,
                ellipsize: Pango.EllipsizeMode.NONE,
            });

            // Make it prompt center
            Main.screenShield._dialog._promptBox.set({
                y_align: Clutter.ActorAlign.CENTER,
            })

            // Remove used padding top of lock hint with message height
            clockStack._hint.set({ style: 'padding-top: 0px;' });
        }

        // When user unlock the screen, the message will automatically destroyed by unlockDialog
    }

    // Nothing we do on enable() method. it started on disable() method
    enable() {
        if (shellVersion >= 42) {
            // Add session mode signal
            if (!this._sessionId) {
                this._sessionId = Main.sessionMode.connect(
                    'updated', this._add.bind(this)
                );
            }
        }
    }

    // Extension starts here
    disable() {
        if (shellVersion >= 42) {
            // Remove session mode signal
            if (this._sessionId) {
                Main.sessionMode.disconnect(this._sessionId);
                delete(this._sessionId);
            }
        } else {
            // Add when user locks the screen and abort when disabled (see _add() method)
            this._add();
        }
    }
}

function init() { return new Extension(); }