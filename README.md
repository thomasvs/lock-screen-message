# Lock Screen Message

Simple GNOME Shell extension that let's you add your message to the lock screen
(unlockDialog)

![Lock Screen Message screenshot](./screenshot.png)

## Story
This extension is made based a story of 
[Creating GNOME Shell Extension in 1 day](https://adeswanta.medium.com/creating-gnome-shell-extension-in-1-day-lock-screen-message-extension-decc4b6ffbef)
## Installation
You can install this extension on [extensions.gnome.org](https://extensions.gnome.org/extension/4801/lock-screen-message/)